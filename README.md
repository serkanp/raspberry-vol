# raspberry-vol 

> Get and set volume in Linux systems.

>since pactl not exists on raspberry, i decided to use amixer feature

## Install

```
$ npm install --save raspberry-vol
```


## Usage

```js
var raspVol = require('raspberry-vol');

raspVol.get(function (err, level) {
	console.log(level);
	//=> 90
});

raspVol.set(90, function (err) {
	console.log('Changed volume to 90%');
});
```


## API

### .get(callback)

Get volume level.

#### callback(err, level)

Type: `function`

##### level

Type: `number`

Current volume level.

### .set(level, callback)

Set volume level.

#### level

*Required*
Type: `number`

A number between `0` and `100`.

#### callback(err)

Type: `function`

Returns nothing but a possible exception.


## CLI

See the [vol](https://bitbucket.org/serkanp/raspberry-vol) CLI.


## License

MIT © [Serkan Polat](http://bitbucket.org/serkanp)
