let clsRaspberry = require('./clsRaspberry')
let rasp = new clsRaspberry();

async function main() {
    let q = await rasp.getDeviceName();
    console.log('setting volume to %100')
    await rasp.setVolume(100)
    let q1 = await rasp.getVolume();
    console.log(q1)
    console.log('setting volume to %50')
    await rasp.setVolume(50)


    let q2 = await rasp.getVolume();
    console.log(q2)


}

main();