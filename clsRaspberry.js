const clsRaspberry = class clsRaspberry {
    constructor() {
        this.device = 'PCM';

    }

    checkLinux() {
        if (process.platform !== 'linux') {
            console.log('Only Linux systems are supported');
            process.exit(9)
        }
    }

    getVolumeCallback(cb){
        this.getDeviceName().then(x=>{
            this.getVolume().then(vol=>{
                if(vol==null)cb(-1)
                cb(parseInt(vol));
            })
        })


    }

    setVolumeCallback(level,cb){
        this.getDeviceName().then(x=>{
            this.setVolume(level).then(()=>{
                cb();
            })
        })
    }

    async getVolume() {
        this.checkLinux();
        const regex = /Playback\s([-0-9]{1,8})\s\[([0-9]{1,3})/gm;
        let cmd=`amixer get ${this.device}`
        //console.log(`running command ${cmd}`)
        let ret = await this.runCommand(cmd)
        if (ret == null) return null;


        let result = this.getregex(ret, regex, 2)
        if (result != null) {
            console.log(`getvolume result is ${result}`)

            return result;
        }
        return null;
    }


    async setVolume(level) {
        this.checkLinux();
        if (typeof level !== 'number') {
            throw new TypeError('Expected a number');
        }

        if (level < 0 || level > 100) {
            console.log('Expected a level between 0 and 1');
            return;
        }
        let cmd=`amixer set ${this.device} -- ${level}%`
        //console.log(`running command ${cmd}`)
        let ret = await this.runCommand(cmd)
    }

    async getDeviceName() {
        this.checkLinux();
        const regex = /Simple mixer control\s'([a-zA-Z]{3,10})'/gm;
        let ret = await this.runCommand('amixer')
        if (ret == null) return null;


        let result = this.getregex(ret, regex, 1)
        if (result != null) {
            console.log(`getDeviceName result is ${result}`)
            this.device = result;
            return result;
        }
        return null;
    }

    runCommand(command) {
        let exec = require('child_process').exec;
        return (new Promise((resolve, reject) => {
            exec(command, (error, stdout, stderr) => {
                if (error) {
                    resolve(null);
                    return;
                }

                resolve(stdout.trim());
            });
        }));
    }

    getregex(msg, regexPattern, index) {

        var matches = regexPattern.exec(msg);

        if (matches === null) {
            return null;
        }
        //console.log(matches)
        return matches[index]
    }

}

module.exports = clsRaspberry